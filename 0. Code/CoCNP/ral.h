#ifndef _RAL_
#define _RAL_

#include <stdio.h>

/*Define the structure*/
typedef struct ST_RandAccessList{
	int *vlist;
	int *vpos;
	int vnum;  // always equal to K
	int capacity;  // vertex number
} RandAccessList;

/***************utils*********************/
RandAccessList* ral_init(int capacity);
void ral_add(RandAccessList *ral, int vid);
void ral_delete(RandAccessList *ral, int vid);
void ral_clear(RandAccessList *ral);
void ral_release(RandAccessList *ral);
int cmpfunc (const void * a, const void * b);
void ral_showList(RandAccessList *ral, FILE *f);

#endif
