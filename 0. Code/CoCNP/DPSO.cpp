#ifndef DPSO_CPP
#define DPSO_CPP

#include "DPSO.h"

using namespace std;

Dpso::Dpso(){

}
Dpso::~Dpso(){
	while(!num_pool.empty()){num_pool.pop();}
    for(int i=0;i<vertex_num;i++)   if(cpn_size[i]>0)   delete [] component[i];
	delete [] component;
	ral_release(cur_solution);
	ral_release(pHistory);
	ral_release(pBest);
	delete [] cpn_size;
	delete [] cpn_list;
	delete [] is_in_solution;
	delete [] weight;
}
void Dpso::Init(int num){
	vertex_num = num;
    is_in_solution = new bool[vertex_num];
    for(int i=0;i<vertex_num;i++) is_in_solution[i] = false;
    component = new int*[vertex_num];
    cpn_list = new int[vertex_num];
    cpn_size = new int[vertex_num];
	weight = new int[vertex_num];
    for(int i=0;i<vertex_num;i++) cpn_size[i] = 0;
    cpn_count = 0;
    cur_solution = ral_init(vertex_num);
	pHistory = ral_init(vertex_num);
	pBest = ral_init(vertex_num);
	connectivity = vertex_num*(vertex_num-1)/2;
	pBest_connectivity = 0;
}

void Dpso::DFS_for_split(vector<int> &haved,int v,bool visited[],int **adjlist,int row_length[]){
	visited[v] = true;  
    haved.push_back(v);
	for(int i=0;i<row_length[v];i++){
		if(!visited[adjlist[v][i]]) DFS_for_split(haved,adjlist[v][i],visited,adjlist,row_length);
	}
}

//initialize the component information
void Dpso::Init_cpn(int **adjlist,int row_length[]){
    while(!num_pool.empty()){num_pool.pop();}
    for(int i=0;i<vertex_num;i++)   if(cpn_size[i]>0)   delete [] component[i];
	for(int i=vertex_num-1;i>=0;i--){
		cpn_size[i] = 0;
		num_pool.push(i);
		is_in_solution[i]=false;
	}
	cpn_count = 0;
	bool visited[MAXV];
	for(int i=0;i<vertex_num;i++){visited[i]=false;}
	for(int i=0;i<vertex_num;i++){
		if(!visited[i]){
			int C = num_pool.top();
			num_pool.pop();
			cpn_count++;
            vector <int> haved;
			DFS_for_split(haved,i,visited,adjlist,row_length);
            cpn_size[C] = haved.size();
            component[C] = new int[cpn_size[C]];
            for(int j=0;j<cpn_size[C];j++){
                component[C][j] = haved[j];
                cpn_list[haved[j]] = C;
            }
		}
	}
}

//put the node from the solution back to the remain figure
void Dpso::put_back(int v,int **adjlist,int row_length[]){
	int C = num_pool.top();
	num_pool.pop();
	cpn_count ++;
	is_in_solution[v] = false;
    ral_delete(cur_solution, v);
    vector <int> haved;
    /*--------------------------combine--------------------------*/
    haved.push_back(v);//put v back to the component
	for(int i=0;i<row_length[v];i++){
		if(is_in_solution[adjlist[v][i]]) continue;
		int C2 = cpn_list[adjlist[v][i]];
		if(cpn_size[C2]==0) continue;
		//combine the set of nodes in the C component and C2 component.
		for(int j=0;j<cpn_size[C2];j++){
            haved.push_back(component[C2][j]);
		}
		num_pool.push(C2);
		cpn_count--;
		cpn_size[C2] = 0;
        delete[] component[C2];
	}
    /*--------------------------finish--------------------------*/
    component[C] = new int[haved.size()];
    cpn_size[C] = haved.size();
    for(int i=0;i<cpn_size[C];i++){
        component[C][i] = haved[i];
        cpn_list[haved[i]] = C;
    }
}
//take the node from the solution out of the remain figure,meaning that get node v to solution
void Dpso::remove_vertex(int v,int **adjlist,int row_length[]){
	int C = cpn_list[v];
	is_in_solution[v] = true;
    ral_add(cur_solution, v);
	bool *visited = new bool[vertex_num];
	for(int i=0;i<vertex_num;i++) visited[i] = is_in_solution[i];
    /*--------------------------split the component--------------------------*/
	for(int i=0;i<row_length[v];i++){
		//Find all connected subgraph ,and update information of all components
		if(!visited[adjlist[v][i]]){
			int C2 = num_pool.top();
			num_pool.pop();
			cpn_count++;
            vector <int> haved;
			DFS_for_split(haved,adjlist[v][i],visited,adjlist,row_length);
            cpn_size[C2] = haved.size();
            component[C2] = new int[cpn_size[C2]];
            for(int j=0;j<cpn_size[C2];j++){
                component[C2][j] = haved[j];
                cpn_list[haved[j]] = C2;
            }
		}
	}
	delete[] visited;
	//delete the components C
	num_pool.push(C);
	cpn_count--;
	cpn_size[C]=0;
    delete[] component[C];
}

//update the history position 
void Dpso::Recorded_history(){
	ral_clear(pHistory);
	for(int i=0;i<cur_solution->vnum;i++)ral_add(pHistory,cur_solution->vlist[i]);
	//pHistory_connectivity = connectivity;
}

#endif