#ifndef DPSO_H
#define DPSO_H

#include <iostream>
#include <string.h>
#include <time.h>
#include <stdlib.h>
#include <assert.h>
#include <stack>
#include <algorithm>
#include <iomanip>
#include "ral.h"
#include <vector>
#include <math.h>

#define MAXV 30000
#define MAXE 200000

using namespace std;

class Dpso{
    public:
      /*-------------------------------member variables-------------------------------*/
        RandAccessList *cur_solution;
        RandAccessList *pHistory;
        RandAccessList *pBest;
        bool *is_in_solution;
        int *cpn_size;//the size of each component
        int **component;
        int *cpn_list;//the nth component containing (resp. near) v if innerv = true (resp. innerv = false).
        //RandAccessList *cpn_neib[MAXV];//the set of nodes near the nth component which is in solution
        int *weight;
        stack <int> num_pool;//unuse number of component
        int cpn_count;
        int connectivity;
        int pBest_connectivity;
        int vertex_num;
        
      /*-------------------------------member funtions-------------------------------*/
        Dpso();
        ~Dpso();
        void Init(int vertex_num);
        void DFS_for_split(vector<int> &haved,int v,bool visited[],int **adjlist,int row_length[]);
        //initialize the component information
        void Init_cpn(int **adjlist,int row_length[]);
        //put the node from the solution back to the remain figure
        void put_back(int v,int **adjlist,int row_length[]);
        //take the node from the solution out of the remain figure,meaning that get node v to solution
        void remove_vertex(int v,int **adjlist,int row_length[]);
        //update the history position 
        void Recorded_history();
      /*-------------------------------test funtions-------------------------------*/
      void print_cpn(int vertex_num){
        int sum[MAXV];
        cout<<"cpn num = "<<cpn_count<<endl;
        for(int i=0;i<=vertex_num;i++){
          sum[i] = 0;
        }
        for(int i=0;i<vertex_num;i++){
          if(cpn_size[i]>0) sum[cpn_size[i]]++;
        }
        cout<<"size of cpn : num of cpn"<<endl;
        for(int i=0;i<=vertex_num;i++){
          if(sum[i]!=0) cout<<i<<"  :  "<<sum[i]<<" |  ";
        }
        cout<<endl;
        for(int i=0;i<vertex_num;i++){
          if(cpn_size[i]==1){
            cout<<component[i][0]<<"  |";
          }
        }
        cout<<endl;
      }
};

#include "DPSO.cpp"

#endif