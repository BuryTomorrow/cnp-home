storm time = 795
WattsStrogatz_n500.txt : Reboot time: 1 ;find best time  = 237.21 ; best conncectivity =2072;
storm time = 1154
WattsStrogatz_n250.txt : Reboot time: 1 ;find best time  = 975.99 ; best conncectivity =3083;
storm time = 32
WattsStrogatz_n1000.txt : Reboot time: 1 ;find best time  = 2592.4 ; best conncectivity =115011;
storm time = 68
WattsStrogatz_n1500.txt : Reboot time: 1 ;find best time  = 306.28 ; best conncectivity =13162;
storm time = 769
WattsStrogatz_n500.txt : Reboot time: 2 ;find best time  = 13.285 ; best conncectivity =2085;
storm time = 1163
WattsStrogatz_n250.txt : Reboot time: 2 ;find best time  = 24.559 ; best conncectivity =3083;
storm time = 68
WattsStrogatz_n1500.txt : Reboot time: 2 ;find best time  = 188.06 ; best conncectivity =13258;
storm time = 33
WattsStrogatz_n1000.txt : Reboot time: 2 ;find best time  = 3592.7 ; best conncectivity =144664;
storm time = 768
WattsStrogatz_n500.txt : Reboot time: 3 ;find best time  = 10.476 ; best conncectivity =2085;
storm time = 1127
WattsStrogatz_n250.txt : Reboot time: 3 ;find best time  = 349.53 ; best conncectivity =3147;
storm time = 31
WattsStrogatz_n1000.txt : Reboot time: 3 ;find best time  = 2835.2 ; best conncectivity =141138;
storm time = 69
WattsStrogatz_n1500.txt : Reboot time: 3 ;find best time  = 571.15 ; best conncectivity =13162;
storm time = 1176
WattsStrogatz_n250.txt : Reboot time: 4 ;find best time  = 107.16 ; best conncectivity =3157;
storm time = 798
WattsStrogatz_n500.txt : Reboot time: 4 ;find best time  = 275.49 ; best conncectivity =2072;
storm time = 36
WattsStrogatz_n1000.txt : Reboot time: 4 ;find best time  = 466.85 ; best conncectivity =143290;
storm time = 69
WattsStrogatz_n1500.txt : Reboot time: 4 ;find best time  = 176.46 ; best conncectivity =13162;
storm time = 1117
WattsStrogatz_n250.txt : Reboot time: 5 ;find best time  = 861.67 ; best conncectivity =3147;
storm time = 773
WattsStrogatz_n500.txt : Reboot time: 5 ;find best time  = 283.34 ; best conncectivity =2085;
storm time = 34
WattsStrogatz_n1000.txt : Reboot time: 5 ;find best time  = 3508.1 ; best conncectivity =144564;
storm time = 68
WattsStrogatz_n1500.txt : Reboot time: 5 ;find best time  = 3380.6 ; best conncectivity =13100;
storm time = 1147
WattsStrogatz_n250.txt : Reboot time: 6 ;find best time  = 739.76 ; best conncectivity =3083;
storm time = 768
WattsStrogatz_n500.txt : Reboot time: 6 ;find best time  = 6.3843 ; best conncectivity =2085;
storm time = 33
WattsStrogatz_n1000.txt : Reboot time: 6 ;find best time  = 2976.1 ; best conncectivity =129879;
storm time = 69
WattsStrogatz_n1500.txt : Reboot time: 6 ;find best time  = 242.64 ; best conncectivity =13098;
storm time = 780
WattsStrogatz_n500.txt : Reboot time: 7 ;find best time  = 2545.7 ; best conncectivity =2072;
storm time = 1152
WattsStrogatz_n250.txt : Reboot time: 7 ;find best time  = 362.48 ; best conncectivity =3083;
storm time = 33
WattsStrogatz_n1000.txt : Reboot time: 7 ;find best time  = 683.5 ; best conncectivity =139666;
storm time = 69
WattsStrogatz_n1500.txt : Reboot time: 7 ;find best time  = 2310.8 ; best conncectivity =13098;
storm time = 1164
WattsStrogatz_n250.txt : Reboot time: 8 ;find best time  = 492.05 ; best conncectivity =3083;

----------------------------------------------------------
File Name : WattsStrogatz_n250.txt ; Run Times : 8
BKV : 0
f_best : 3083
f_avg : 3108.2
time_avg of find solution : 489.15
time_avg of find min solution : 518.97
succ_time = 5
----------------------------------------------------------

storm time = 769
WattsStrogatz_n500.txt : Reboot time: 8 ;find best time  = 184 ; best conncectivity =2085;

----------------------------------------------------------
File Name : WattsStrogatz_n500.txt ; Run Times : 8
BKV : 0
f_best : 2072
f_avg : 2080.1
time_avg of find solution : 444.48
time_avg of find min solution : 1019.5
succ_time = 3
----------------------------------------------------------

storm time = 30
WattsStrogatz_n1000.txt : Reboot time: 8 ;find best time  = 1823.1 ; best conncectivity =133412;

----------------------------------------------------------
File Name : WattsStrogatz_n1000.txt ; Run Times : 8
BKV : 0
f_best : 115011
f_avg : 1.3645e+05
time_avg of find solution : 2309.7
time_avg of find min solution : 2592.4
succ_time = 1
----------------------------------------------------------

storm time = 70
WattsStrogatz_n1500.txt : Reboot time: 8 ;find best time  = 755.04 ; best conncectivity =13098;

----------------------------------------------------------
File Name : WattsStrogatz_n1500.txt ; Run Times : 8
BKV : 0
f_best : 13098
f_avg : 13142
time_avg of find solution : 991.39
time_avg of find min solution : 1102.8
succ_time = 3
----------------------------------------------------------

