#include <iostream>
#include <sstream>
#include <fstream>
#include <string.h>
#include <vector>
#include <time.h>
#include <stdlib.h>
#include <assert.h>
#include <stack>
#include <algorithm>
#include <iomanip>
#include "DPSO.h"
#include "ral.h"
#include <math.h>       /* exp */

#define MAXV 30000
#define MAXE 200000
#define NIL -1

using namespace std;

/* hyper-parameter */
#define STUCK_LIMIT 10
/*Two PSO update probability constants*/
#define PROBABILITY_P 80
#define	PROBABILITY_G 80
/*the probability of avoid the solution obtained by the scouts */
//#define AVOID_PROBABILITY 25
/*a multiple of (vertex num / K)*/
//#define MULTIPLE 5
//#define MAXIDLEITERS 1000
double time_avg;
int f_best;

int sum_step = 0;

struct Children_Info{
	int children_size;
	int min_low;
};

struct N1_Type{
	bool is_ap;
	int node;
	int low;
	int disc;
	int com_size;
	int influence;
	vector <Children_Info> children;
};
/*N1 value for each node*/
N1_Type N1_info[MAXV];
int disc_time = 0; 
/*N1 value for each node,N1_value[i]==0 means not ap */
int N1_value[MAXV];
int N1_value_index[MAXV];
/* time */
clock_t start_time;
float best_time;
float time_out = 1000;  // 1000 seconds
float find_best_time;
int find_best_step;
int steps = 0;
/* graph description */
int vertex_num, edge_num, K;
int **adjlist;  // adjacency list of graph
int row_length[MAXV];  // number of vertices of each vertex

/* currrent solution */
Dpso *Particle;
Dpso Scout;
int Par_Num;
/* best solution */
int *best_solution;
RandAccessList *gBest;
int gBest_connectivity;
/* connectivity */
int max_connectivity;  // set to |V|*(|V|-1)/2
int min_connectivity;
int cur_connectivity;  // connectivity of current solution
int local_optima_connectivity;  // track local optima
int max_g_influence_node; // max influence node of the remain figure  
int max_g_influence;//the influence of the max influence node 

double time_cost() {
	return (double)(clock() - start_time) / CLOCKS_PER_SEC;
}

bool timeout() {
	return time_cost() > time_out;
}

// ----------------------------- IO ---------------------------------
void print_array(int len = K, int a[] = best_solution) {
	int *temp = new int[vertex_num];
	for(int i=0;i<vertex_num;i++) temp[i]=a[i];
	sort(temp,temp+len);
	for (int i = 0; i < len; ++i)
		printf("%d ", temp[i]);
	printf("\n");
	delete [] temp;
}

void print_solution() {
	sort(best_solution, best_solution+K);
	print_array();
}

void print_adjlist() {
	int i = 0;
	while (adjlist[i]) {
		printf("%d: ", i);
		print_array(row_length[i], adjlist[i]);
		i++;
	}
}

void read_adjlist(char* File_Name) {
	ifstream FIC;
	FIC.open(File_Name);
	if ( FIC.fail() ) {
	   printf("### Erreur open, File_Name %s", File_Name);
	   getchar();
	   exit(0);
	}
	if ( FIC.eof() ) {
	   printf("### Error open, File_Name %s", File_Name);
	   exit(0);
	}

	int endofline;
	FIC >> vertex_num >> edge_num >> endofline;

	adjlist = new int*[vertex_num];

	string line;
	for (int line_num = 0; line_num < vertex_num; ++line_num) {
		getline(FIC, line);
		if (line.substr(line.find(':')+1).length()<2) {
			adjlist[line_num] = new int[0];
			row_length[line_num]=0;continue;
		}
		istringstream iss(line);
		string head_colon;
		iss >> head_colon;
		vector<int> line_vector;
		int tmp_vertex;
		iss >> tmp_vertex;
		do {
			line_vector.push_back(tmp_vertex);
		} while (iss >> tmp_vertex);

		adjlist[line_num] = new int[line_vector.size()];
		for (size_t i = 0; i < line_vector.size(); ++i)
			adjlist[line_num][i] = line_vector[i];
		row_length[line_num] = line_vector.size();
	}
}
// ----------------------------- IO ---------------------------------

int randInt(int n) {return rand() % n; }

int DFS(int v, bool visited[]) {
	visited[v] = true;
	int component_size = 1;
    for(int i = 0; i < row_length[v]; ++i) 
        if(!visited[adjlist[v][i]])
            component_size += DFS(adjlist[v][i], visited);
    return component_size;
}

// trick here: remove nodes in solution is equal to set those nodes visited
int get_connectivity(int visited_nodes[], int num ) {
	int connectivity = 0;
	bool *visited = new bool[vertex_num];

	// initialization
    for (int i = 0; i < vertex_num; ++i) 
        visited[i] = false;
    // remove nodes
    for (int i = 0; i < num; ++i) 
        visited[visited_nodes[i]] = true;

    // traverse the forest
    for (int i = 0; i < vertex_num; ++i) {
    	int component_size = 0;
        if (visited[i] == false) {
        	// traverse a single graph
            component_size = DFS(i, visited);
        }
        connectivity += component_size * (component_size-1) / 2;
    }

    delete[] visited;
    return connectivity;
}

Children_Info dfs_N1(int u, bool *visited, int *parent,vector <int> &haved){
	// Count of children in DFS Tree 
	int children = 0;
	// Mark the current node as visited 
	visited[u] = true; 
	haved.push_back(u);
	// Initialize discovery time and low value 
	N1_info[u].disc = N1_info[u].low = ++disc_time; 
	// Go through all vertices aadjacent to this 
	Children_Info sum;
	sum.children_size = 1;//include u 
	sum.min_low = MAXV;
	for (int i = 0; i < row_length[u]; ++i) {
		int v = adjlist[u][i]; // v is current adjacent of u 

		// If v is not visited yet, then make it a child of u 
		// in DFS tree and recur for it 
		if (!visited[v]) {
			children++; 
			parent[v] = u; 
			//find the children information
			Children_Info temp=dfs_N1(v, visited,parent,haved);
			sum.children_size+=temp.children_size;
			sum.min_low = min(sum.min_low,temp.min_low);

			//Keep track of a child
			N1_info[u].children.push_back(temp);

			// Check if the subtree rooted with v has a connection to 
			// one of the ancestors of u 
			N1_info[u].low = min(N1_info[u].low, N1_info[v].low); 

			// u is an articulation point in following cases 

			// (1) u is root of DFS tree and has two or more chilren. 
			if (parent[u] == NIL && children > 1)
				N1_info[u].is_ap = true; 

			// (2) If u is not root and low value of one of its child is more 
			// than discovery value of u. 
			if (parent[u] != NIL && N1_info[v].low >= N1_info[u].disc)
				N1_info[u].is_ap = true;
		}
		// Update low value of u for parent function calls.
		else if (v != parent[u]){
			vector<int>::iterator find1=find(haved.begin(),haved.end(),v);
			if(find1!=haved.end()) N1_info[u].low = min(N1_info[u].low, N1_info[v].disc); 
		}
			
	}
	sum.min_low=min(sum.min_low,N1_info[u].low);
	return sum;
}

/*Calculate influence degree*/
int Cal_influence(int v){return v*(v-1)/2;}

/*Get the most influential point,return the node value*/
int find_N1(bool visited[],int &max_influence){
	int parent[MAXV];
	// Initialize parent and visited, and ap(articulation point) arrays 
	for (int i = 0; i < vertex_num; ++i) { 	//O(|V|)
		parent[i] = NIL; 
		N1_info[i].children.clear();
		N1_info[i].com_size=0;
		N1_info[i].disc=0;
		N1_info[i].influence = 0;
		N1_info[i].low =0;
		N1_info[i].is_ap=false;
	}
	//calculate the N1 information
	for(int i=0;i<vertex_num;i++){	//O(|V|+|E|)
		if(!visited[i]){
			disc_time = 0;
			vector <int> haved;//the point in the same component
			Children_Info root = dfs_N1(i,visited,parent,haved);
			//Record the size of the subgraph where each point is located
			for(unsigned int j=0;j<haved.size();j++)
				N1_info[haved[j]].com_size = root.children_size;
		}
	}
	//get the influence and find the max one
	int max_influence_node = -1;
	max_influence = 0;
	for(int i=0;i<vertex_num;i++){	//lower than O(|V|+|E|)
		//if(!N1_info[i].is_ap) continue;	//only calculate the ap node
		int sub_influence = 0;
		int sum_of_real_tree = 0;
		for(unsigned int j=0;j<N1_info[i].children.size();j++){
			//real subtree,meaning that None of the points in the subtree are connected to the upper parent
			if(N1_info[i].children[j].min_low>=N1_info[i].disc){
				sub_influence += Cal_influence(N1_info[i].children[j].children_size);
				sum_of_real_tree += N1_info[i].children[j].children_size;
			}
		}
		N1_info[i].influence = Cal_influence(N1_info[i].com_size) - sub_influence - Cal_influence(N1_info[i].com_size-1-sum_of_real_tree);
		if(N1_info[i].influence>max_influence){
			max_influence = N1_info[i].influence;
			max_influence_node = i;
		}
	}
	return max_influence_node;
}

void update_influence(Dpso &P){
	bool *visited = new bool[vertex_num];
	for(int i=0;i<vertex_num;i++){visited[i]=P.is_in_solution[i];}
	max_g_influence = 0;
	max_g_influence_node = find_N1(visited,max_g_influence);
	delete [] visited;
}

void print_cpn(Dpso &P){
	int sum[MAXV];
	cout<<"cpn num = "<<P.cpn_count<<endl;
	for(int i=0;i<=vertex_num;i++){
		sum[i] = 0;
	}
	for(int i=0;i<vertex_num;i++){
		if(P.cpn_size[i]>0) sum[P.cpn_size[i]]++;
	}
	cout<<"size of cpn : num of cpn"<<endl;
	for(int i=0;i<=vertex_num;i++){
		if(sum[i]!=0) cout<<i<<"  :  "<<sum[i]<<" |  ";
	}
	cout<<endl;
	for(int i=0;i<vertex_num;i++){
		if(P.cpn_size[i]==1){
			cout<<P.component[i][0]<<"  |";
		}
	}
	cout<<endl;
}

// --------------------- restart from brand new solution -----------------------

void init_rand_solution(RandAccessList *candidate,Dpso &P){
	while(P.cur_solution->vnum<K){
		int vrand = randInt(candidate->vnum);
		while(P.cur_solution->vpos[candidate->vlist[vrand]]!=-1)  vrand = randInt(candidate->vnum);
		P.remove_vertex(candidate->vlist[vrand],adjlist,row_length);
		ral_delete(candidate,candidate->vlist[vrand]);
	}
	P.connectivity = get_connectivity(P.cur_solution->vlist,K);
}

void restart() {
	// initalize every particle's cur_solution and component
	RandAccessList *candidate;
	candidate = ral_init(vertex_num);
	for(int i=0;i<vertex_num;i++) if(row_length[i]>1) ral_add(candidate,i);
	ral_clear(gBest);
	gBest_connectivity = max_connectivity;

	//ral_clear(Scout.cur_solution);
	//Scout.Init_cpn(adjlist,row_length);
	//init_rand_solution(candidate,Scout);

	for(int i=0;i<Par_Num;i++){
		if(candidate->vnum<2*K){
			ral_clear(candidate);
			for(int j=0;j<vertex_num;j++) if(row_length[j]>1) ral_add(candidate,j);
		}
		/*------------------add vertex to every particle------------------*/
		ral_clear(Particle[i].cur_solution);
		Particle[i].Init_cpn(adjlist,row_length);
		init_rand_solution(candidate,Particle[i]);
	}
	ral_release(candidate);
}
// -----------------------------------------------------------------------------

void initialization() {
	srand(time(0));
	max_connectivity = vertex_num*(vertex_num-1)/2;
	// initalize best solution
	best_solution = new int[vertex_num];
	memset(best_solution, false, sizeof(int) * K);
	gBest = ral_init(vertex_num);
	//Par_Num = vertex_num/K ;

	//Par_Num = MULTIPLE*Par_Num;

	Par_Num = 15;

	Particle = new Dpso[Par_Num];
	for(int i=0;i<Par_Num;i++){
		Particle[i].Init(vertex_num);
	}
	Scout.Init(vertex_num);
	min_connectivity = max_connectivity;
	// initalize every particle's cur_solution and component
	restart();
}

/* update best connectivity and corresponding time once finding better solution */
void update_best() {
	best_time = time_cost();
	min_connectivity = gBest_connectivity;
	for (int i = 0; i < gBest->vnum; i++) {
		best_solution[i] = gBest->vlist[i];
	}
}

// --------------------------- local search part ----------------------------

int get_h_influence(int node,Dpso &P){
	int Component_num[MAXV],Cpn_num_size[MAXV];
	int h_influence = 0,h_sum = 1;
	int S=0;
	for(int j=0;j<row_length[node];j++){		//smaller than O(|V|)
		int neib = adjlist[node][j];
		if(P.is_in_solution[neib]) continue;
		if(P.cpn_size[P.cpn_list[neib]] == 0) continue;
		Component_num[S]=P.cpn_list[neib];
		Cpn_num_size[S]=P.cpn_size[P.cpn_list[neib]];
		S++;
		h_sum += P.cpn_size[P.cpn_list[neib]];
		h_influence += Cal_influence(P.cpn_size[P.cpn_list[neib]]);
		P.cpn_size[P.cpn_list[neib]]=0;
	}
	//smaller than O(|V|)
	for(int j=0;j<S;j++){ P.cpn_size[Component_num[j]]=Cpn_num_size[j]; }
	h_influence = Cal_influence(h_sum) - h_influence;
	return h_influence;
}

/* swap method */
bool to_a_best_solution(Dpso &P) {
	int optima_after_swap = P.connectivity;
	int delete_node = -1;
	int add_node = -1;

	bool visited[MAXV];
	update_influence(P);
	int influence_of_V_S[MAXV];
	for(int i=0;i<vertex_num;i++) influence_of_V_S[i]=N1_info[i].influence;
	//cout<<endl<<"-----------------"<<sum_step<<"----------------------"<<endl;
	for(int i = 0; i < K; ++ i) {
		int op_node = P.cur_solution->vlist[i];
		/*-------------------------------incremental updating---------------------------------*/
		/*-------------------------h(v)------------------------------*/
		int h_influence = get_h_influence(op_node,P);	//O(Degree(op_node));
		/*-------------------------g(v)------------------------------*/
		int parent[MAXV];
		// Initialize parent and visited, and ap(articulation point) arrays 
		for (int j = 0; j < vertex_num; ++j) { 	//O(|V|)
			visited[j]=false;
			parent[j] = NIL; 
			N1_info[j].children.clear();
			N1_info[j].com_size=0;
			N1_info[j].disc=0;
			N1_info[j].low =0;
			N1_info[j].influence = 0;
			N1_info[j].is_ap=false;
		}
		for(int j=0;j<K;j++){visited[P.cur_solution->vlist[j]]=true;}	//O(|K|)
		disc_time = 0;
		vector <int> haved;//the point in the same component
		Children_Info root = dfs_N1(op_node,visited,parent,haved); // O(|Vc|+|Ec|)

		int g_influence_node = -1;
		int g_influence = -1;
		bool same_cpn = false;
		
		for(unsigned int w=0;w<haved.size();w++){	//smaller than  O(2*|Vc|)
			if(haved[w]!=op_node&&max_g_influence_node!=-1){
				if(P.cpn_list[haved[w]]==P.cpn_list[max_g_influence_node]) same_cpn = true;
			}
			int sub_influence = 0;
			int sum_of_real_tree = 0;
			for(unsigned int j=0;j<N1_info[haved[w]].children.size();j++){
				//real subtree,meaning that None of the points in the subtree are connected to the upper parent
				if(N1_info[haved[w]].children[j].min_low>=N1_info[haved[w]].disc){
					sub_influence += Cal_influence(N1_info[haved[w]].children[j].children_size);
					sum_of_real_tree += N1_info[haved[w]].children[j].children_size;
				}
			}
			N1_info[haved[w]].influence = Cal_influence(root.children_size) - sub_influence - Cal_influence(root.children_size-1-sum_of_real_tree);
			if(N1_info[haved[w]].influence>g_influence){
				g_influence = N1_info[haved[w]].influence;
				g_influence_node = haved[w];
			}
		}
		int best_node = -1;
		if(same_cpn){ // int the same component
			// avoid the loop mistake
			for(int j=0;j<vertex_num;j++){
				//fing the max influence which not belong to tmp_solution[i]'s neighbor cpn
				if(influence_of_V_S[j]>g_influence&&!P.is_in_solution[j]){
					bool f = true;
					for(unsigned int u = 0 ;u<haved.size();u++) 
						if(haved[u]!=op_node&&P.cpn_list[j]==P.cpn_list[haved[u]]) f = false;
					if(!f) continue;
					g_influence_node = j;
					g_influence = influence_of_V_S[j];
				}
			}
			best_node = g_influence_node;
			if(g_influence<=0){
				best_node = op_node;
				g_influence = h_influence;
			}
		}else{
			best_node = g_influence>max_g_influence? g_influence_node : max_g_influence_node;
			g_influence = max(max_g_influence,g_influence);
		}
		//calculate the influence if exchange the pair of nodes
		if(best_node==-1){
			best_node=op_node;
			g_influence = h_influence;
		}
		int update_connectivity = max_connectivity;
		update_connectivity = P.connectivity+h_influence-g_influence;
		if (update_connectivity < optima_after_swap) {
			optima_after_swap = update_connectivity;
			delete_node = op_node;
			add_node = best_node;
		}
	}
	if(optima_after_swap >= P.connectivity) return false;
	P.put_back(delete_node,adjlist,row_length);
	P.remove_vertex(add_node,adjlist,row_length);
	P.connectivity = optima_after_swap;

	/*--------------------------test--------------------------*/
	// if(P.connectivity!=get_connectivity(P.cur_solution->vlist,K)){
	// 	cout<<"wrong!!!!"<<local_optima_connectivity<<"  "<<get_connectivity(P.cur_solution->vlist,K)<<endl;
	// 	cout<<delete_node<<"  "<<add_node<<endl;
	// }
	return true;
}

bool to_a_greedy_solution(Dpso &P){
	int optima_connectivity = P.connectivity;
	/*---------------------------add---------------------------*/
	bool *visited = new bool[vertex_num];
	for(int i=0;i<vertex_num;i++){visited[i]=P.is_in_solution[i];}
	int g_influence = 0,g_influence_node = find_N1(visited,g_influence);//O(|V|+|E|)
	delete [] visited; 
	if(g_influence_node == -1) return false;
	P.remove_vertex(g_influence_node,adjlist,row_length);
	optima_connectivity-=g_influence;
	/*---------------------------remove---------------------------*/
	int h_influence = max_connectivity,h_influence_node = -1;
	//O(K*max_Degree(solution_node));
	for(int i=0;i<P.cur_solution->vnum;i++){
		int temp = get_h_influence(P.cur_solution->vlist[i],P);//O(Degree(op_node))
		if(temp<=h_influence){
			h_influence = temp;
			h_influence_node = P.cur_solution->vlist[i];
		}
	}
	P.put_back(h_influence_node,adjlist,row_length);
	optima_connectivity+=h_influence;

	if(optima_connectivity>=P.connectivity){
		P.remove_vertex(h_influence_node,adjlist,row_length);
		P.put_back(g_influence_node,adjlist,row_length);
		return false;
	}
	P.connectivity = optima_connectivity;
	/*---------------------------check---------------------------*/
	// if(P.connectivity!=get_connectivity(P.cur_solution->vlist,K)){
	// 	cout<<"wrong!!!!"<<local_optima_connectivity<<"  "<<get_connectivity(P.cur_solution->vlist,K)<<endl;
	// }
	return true;
} 

//find the node which have the max degree in the component
int find_max_degree(int cpn_index,Dpso &P){
	int v = P.component[cpn_index][0],degree = 0;
	for(int i=0;i<P.cpn_size[cpn_index];i++){
		int u = P.component[cpn_index][i];
		int d = 0;
		for(int j=0;j<row_length[u];j++){
			if(!P.is_in_solution[adjlist[u][j]]) d++;
		}
		if(d>degree){
			v = u;
			degree = d;
		}
	}
	return v;
}

void get_rand_node_and_add(int cpn,Dpso &P){
	// int remove_node = P.component[cpn][0];
	// for(int i=0;i<P.cpn_size[cpn];i++){
	// 	if(P.weight[P.component[cpn][i]]>P.weight[remove_node]){
	// 		remove_node = P.component[cpn][i];
	// 	}
	// 	else if(P.weight[P.component[cpn][i]]==P.weight[remove_node]){
	// 		if(row_length[P.component[cpn][i]]>row_length[remove_node]){
	// 			remove_node = P.component[cpn][i];
	// 		}
	// 	}
	// }
	// P.remove_vertex(remove_node,adjlist,row_length);
	int vrand = randInt(P.cpn_size[cpn]);
	while (P.is_in_solution[P.component[cpn][vrand]]) vrand = randInt(P.cpn_size[cpn]);
	P.remove_vertex(P.component[cpn][vrand],adjlist,row_length);
}

int find_max_cpn(Dpso &P){
	int max_cpn = -1,max_size = 0;
	for(int i=0;i<vertex_num;i++){
		if(P.cpn_size[i]>max_size){
			max_size = P.cpn_size[i];
			max_cpn = i;
		}
	}
	return max_cpn;
}

int find_rand_large_cpn(Dpso &P){
	vector<int> large;
	int min = vertex_num , max = 0,sum = 0;
	//
	for(int i=0;i<vertex_num;i++){
		if(P.cpn_size[i]<min) min = P.cpn_size[i];
		if(P.cpn_size[i]>max) max = P.cpn_size[i];
		sum+=P.cpn_size[i];
	}
	//
	//int L = sum/P.cpn_count;
	int L = (max+min)/2;
	for(int i=0;i<vertex_num;i++){
		if(P.cpn_size[i]>L){
			large.push_back(i);
		}
	}
	int r = randInt(large.size());
	return large[r];
}

int local_search_time = 0;

void shock(Dpso &P,int stuck_time){
	int last_cpn_count = P.cpn_count,extra_num = 0;
		//Cutting the maximally connected subgraph,randomly
		while(P.cpn_count<=last_cpn_count+pow(2,min(stuck_time,10))){
			if(timeout()) break;
			int max_cpn = find_max_cpn(P);
			if(P.cpn_size[max_cpn]<=2) break;
			extra_num++;
			get_rand_node_and_add(max_cpn,P);
			/*---------------------------------*/
			for(int i=0;i<P.cpn_size[max_cpn];i++) P.weight[P.component[max_cpn][i]]++;
		}
		//////////////////////////////////////////////////////////////////////////
		while(extra_num>=2){
			if(timeout()) break;
			int h_influence = max_connectivity;
			int delete_node_1 = P.cur_solution->vlist[0];
			int delete_node_2 = P.cur_solution->vlist[1];
			for(int i=P.cur_solution->vnum-1;i>=1;i--){
				int temp_1 = get_h_influence(P.cur_solution->vlist[i],P);
				int temp_delete = P.cur_solution->vlist[i];
				P.put_back(temp_delete,adjlist,row_length);
				for(int j=i-1;j>=0;j--){
					int temp_2 = get_h_influence(P.cur_solution->vlist[j],P);
					if(temp_1+temp_2<h_influence){
						h_influence = temp_1+temp_2;
						delete_node_1 = temp_delete;
						delete_node_2 = P.cur_solution->vlist[j];
					}
				}
				P.remove_vertex(temp_delete,adjlist,row_length);
			}
			P.put_back(delete_node_1,adjlist,row_length);
			P.put_back(delete_node_2,adjlist,row_length);
			/*------------------------------------------*/
			P.weight[delete_node_1] = 0;
			P.weight[delete_node_2] = 0;
			extra_num-=2;
		}
		//////////////////////////////////////////////////////////////////////////
		while(extra_num--){
			if(timeout()) break;
			int h_influence = Cal_influence(vertex_num),delete_node = -1;
			for(int i=0;i<P.cur_solution->vnum;i++){
				int temp = get_h_influence(P.cur_solution->vlist[i],P);
				if(temp<=h_influence){
					h_influence = temp;
					delete_node = P.cur_solution->vlist[i];
				}
			}
			P.put_back(delete_node,adjlist,row_length);
			/*------------------------------------------*/
			P.weight[delete_node] = 0;
		}
		P.connectivity=get_connectivity(P.cur_solution->vlist,K);
}

void clear_weight(Dpso &P){
	for(int i=0;i<vertex_num;i++) P.weight[i] = 0;
}

//perform a local search
void ComponentBasedNeighborhoodSearch(Dpso &P){
	int iter = 0,idle_iter = 0;
	ral_clear(P.pBest);
	for(int i=0;i<P.cur_solution->vnum;i++) ral_add(P.pBest,P.cur_solution->vlist[i]);
	P.pBest_connectivity = P.connectivity;
	//clear_weight(P);
	while(idle_iter<2*vertex_num&&!timeout()){
		//select a large component cpn at random
		int cpn = find_rand_large_cpn(P);
		//int cpn = find_max_cpn(P);
		//remove a node v from cpn with the node weighting scheme;
		int remove_node = P.component[cpn][0],node_weight = P.weight[P.component[cpn][0]];
		int max_degree = row_length[P.component[cpn][0]];
		/*--------------------------remove--------------------------*/
		for(int i=0;i<P.cpn_size[cpn];i++){
			if(P.weight[P.component[cpn][i]]>node_weight){
				remove_node = P.component[cpn][i];
				node_weight = P.weight[P.component[cpn][i]];
				max_degree = row_length[P.component[cpn][i]];
			}else if(P.weight[P.component[cpn][i]]==node_weight){
				if(row_length[P.component[cpn][i]]>max_degree){
					max_degree = row_length[P.component[cpn][i]];
					remove_node = P.component[cpn][i];
				}
			}
		}
		for(int i=0;i<P.cpn_size[cpn];i++){
			if(P.component[cpn][i]==remove_node) continue;
			P.weight[P.component[cpn][i]]++;
		}
		P.remove_vertex(remove_node,adjlist,row_length);
		P.connectivity -= get_h_influence(remove_node,P);
		/*--------------------------put back--------------------------*/
		int h_influence = Cal_influence(vertex_num),put_back_node = P.cur_solution->vlist[0];
		for(int i=0;i<P.cur_solution->vnum-1;i++){
			int temp = get_h_influence(P.cur_solution->vlist[i],P);
			if(temp<=h_influence){
				h_influence = temp;
				put_back_node = P.cur_solution->vlist[i];
			}
		}
		P.put_back(put_back_node,adjlist,row_length);
		P.weight[put_back_node] = 0;
		P.connectivity += h_influence;
		/*-------check-------*/
		//if(P.connectivity!=get_connectivity(P.cur_solution->vlist,K)){cout<<"IS FALSE!!!!!";}
		if(P.connectivity<P.pBest_connectivity){
			ral_clear(P.pBest);
			for(int i=0;i<P.cur_solution->vnum;i++) ral_add(P.pBest,P.cur_solution->vlist[i]);
			P.pBest_connectivity = P.connectivity;
			idle_iter = 0;
		}else{
			idle_iter++;
		}
		iter++;
	}
}
//explore before the Particle swarm optimization
void Scout_explore(){
	int stuck_time = 0;  // if 0, which means no improvment, stop
	while(stuck_time!=STUCK_LIMIT && !timeout()){
		/*-----------------------------Greedy phase-----------------------------*/
		//while(to_a_best_solution(Scout)){}
		ComponentBasedNeighborhoodSearch(Scout);
		if(Scout.pBest_connectivity<min_connectivity){
			ral_clear(gBest);
			gBest_connectivity = Scout.pBest_connectivity;
			for(int i=0;i<Scout.pBest->vnum;i++){
				ral_add(gBest,Scout.pBest->vlist[i]);
			}
			update_best();
			find_best_time = time_cost();
			find_best_step = steps;
			stuck_time = 0;
		}else{
			stuck_time++;
		}
		/*-----------------------------Storming phase-----------------------------*/
		//shock(Scout,stuck_time);
	}
}
bool local_search() {
	bool improve = false;
	int stuck_time = 0;  // if 0, which means no improvment, stop
	while (stuck_time!=STUCK_LIMIT && !timeout()) {
		/*-----------------------------PSO-----------------------------*/
		//steps++;
		bool flag = false;
		//int Best_Num = 0;
		//ral_clear(gBest);
		//gBest_connectivity = max_connectivity;
		for(int i=0;i<Par_Num;i++){
			if(timeout()) break;
			//record the particle's position 
			Particle[i].Recorded_history();
			//to_a_greedy_solution(Particle[i]);
			//while(to_a_best_solution(Particle[i])){if(timeout()) break;steps++;}
			ComponentBasedNeighborhoodSearch(Particle[i]);
			while(to_a_best_solution(Particle[i])&&!timeout());
			if(Particle[i].connectivity<Particle[i].pBest_connectivity){
				Particle[i].pBest_connectivity = Particle[i].connectivity;
				ral_clear(Particle[i].pBest);
				for(int j=0;j<Particle[i].cur_solution->vnum;j++){
					ral_add(Particle[i].pBest,Particle[i].cur_solution->vlist[j]);
				}
			}
			steps++;
			if(Particle[i].pBest_connectivity<gBest_connectivity){
				gBest_connectivity = Particle[i].pBest_connectivity;
				ral_clear(gBest);
				for(int j=0;j<Particle[i].pBest->vnum;j++){
					ral_add(gBest,Particle[i].pBest->vlist[j]);
				}
			}
			if(gBest_connectivity<min_connectivity){
				update_best();
				find_best_time = time_cost();
				find_best_step = steps;
				flag = true;
				if(min_connectivity<=f_best) return true;
			}
		}
		//update global best solution
		if(flag) stuck_time = 0;
		else stuck_time++;
		// stuck_time++;
		/*-----------------------------update funtion-----------------------------*/
		for(int i=0;i<Par_Num;i++){
			if(timeout()) break;
			//Xcommon = pHistory ∩ pBest ∩ gBest;  
			//Xexclusive_p = (pHistory ∪ pBest ∪ gBest) - gBest
			//Xexclusive_g = (pHistory ∪ pBest ∪ gBest) - (pHistory ∩ pBest)
			//Xexcluding = All_vertex - Xa 
			RandAccessList *Xcommon,*Xdifference,*Xexclusive_p,*Xexclusive_g,*Xexcluding;
			Xcommon = ral_init(vertex_num);
			Xdifference = ral_init(vertex_num);
			Xexclusive_p = ral_init(vertex_num);
			Xexclusive_g = ral_init(vertex_num);
			Xexcluding = ral_init(vertex_num);
			/*-----------------update every particle's cur_solution------------------*/
			//calculate the subset 
			for(int j=0;j<vertex_num;j++){
				//Xcommon = pHistory ∩ pBest ∩ gBest;
				if(Particle[i].pHistory->vpos[j]!=-1&&Particle[i].pBest->vpos[j]!=-1&&gBest->vpos[j]!=-1){
					ral_add(Xcommon,j);
				}
				//Xexclusive_p = (pHistory ∪ pBest ∪ gBest) - gBest
				else if(Particle[i].pHistory->vpos[j]!=-1||Particle[i].pBest->vpos[j]!=-1){
					ral_add(Xexclusive_p,j);
					ral_add(Xdifference,j);
				}
				//Xexclusive_g = (pHistory ∪ pBest ∪ gBest) - (pHistory ∩ pBest)
				else if(gBest->vpos[j]!=-1){
					ral_add(Xexclusive_g,j);
					ral_add(Xdifference,j);
				}
				//Xexcluding = All_vertex - Xa 
				else{
					ral_add(Xexcluding,j);
					ral_add(Xdifference,j);
				}
			}
			//Delete differences
			for(int j=0;j<Xdifference->vnum;j++){
				if(Particle[i].cur_solution->vpos[Xdifference->vlist[j]]!=-1)
					Particle[i].put_back(Xdifference->vlist[j],adjlist,row_length);
			}
			//get node 
			for(int j=0;j<Xexclusive_p->vnum;j++){
				//if(!avoid_or_accept(Xexclusive_p->vlist[j])) continue;
				int R = randInt(100);
				if(R<PROBABILITY_P) Particle[i].remove_vertex(Xexclusive_p->vlist[j],adjlist,row_length);
			}
			for(int j=0;j<Xexclusive_g->vnum;j++){
				//if(!avoid_or_accept(Xexclusive_g->vlist[j])) continue;
				int R = randInt(100);
				if(R<PROBABILITY_G) Particle[i].remove_vertex(Xexclusive_g->vlist[j],adjlist,row_length);
			}
			//Restore the size of the solution to K
			while(Particle[i].cur_solution->vnum<K){
				get_rand_node_and_add(find_max_cpn(Particle[i]),Particle[i]);
			}
			while(Particle[i].cur_solution->vnum>K){
				int h_influence = Cal_influence(vertex_num),delete_node = -1;
				for(int j=0;j<Particle[i].cur_solution->vnum;j++){
					int temp = get_h_influence(Particle[i].cur_solution->vlist[j],Particle[i]);
					if(temp<=h_influence){
						h_influence = temp;
						delete_node = Particle[i].cur_solution->vlist[j];
					}
				}
				Particle[i].put_back(delete_node,adjlist,row_length);
			}
			Particle[i].connectivity = get_connectivity(Particle[i].cur_solution->vlist,K);

			ral_release(Xcommon);
			ral_release(Xdifference);
			ral_release(Xexclusive_p);
			ral_release(Xexclusive_g);
			ral_release(Xexcluding);
		}
	}
	return improve;
}
// -----------------------------------------------------------------------------

//update the component information
void update_cpn_info(int &avg,int &max_size,int &max_size_index,Dpso &P){
	int num = 0;
	for(int i=0;i<vertex_num;i++){
		avg+=P.cpn_size[i];
		if(P.cpn_size[i]!=0) num++;
		if(P.cpn_size[i]>max_size){
			max_size = P.cpn_size[i];
			max_size_index = i;
		}
	}
	avg /= num;
}

//Make the size of all connected subgraphs average
void average_cpn_size(Dpso &P){
	int num_of_put_back = 0;
	for(int i=0;i<K;i++){
		//calculate the average and find the max cpmponent
		int avg_size = 0,max_size = 0,max_size_index = -1;
		update_cpn_info(avg_size,max_size,max_size_index,P);
		int v = P.cur_solution->vlist[i],near_cpn_size = 0;
		vector<int> haved; vector<int> haved_size;
		for(int j=0;j<row_length[v];j++){
			int u = adjlist[v][j];
			if(!P.is_in_solution[u]&&P.cpn_size[P.cpn_list[u]]!=0){
				near_cpn_size+= P.cpn_size[P.cpn_list[u]];
				haved.push_back(P.cpn_list[u]);
				haved_size.push_back(P.cpn_size[P.cpn_list[u]]);
				P.cpn_size[P.cpn_list[u]] = 0;
			}
		}
		for(unsigned int j = 0;j<haved.size();j++) P.cpn_size[haved[j]] = haved_size[j];
		//if lower than the avg,put the node back to the graph
		if(near_cpn_size+1<=max_size){
			num_of_put_back++;
			P.put_back(v,adjlist,row_length);
		}
	}
	while(num_of_put_back--){
		int avg_size = 0,max_size = 0,max_size_index = -1;
		update_cpn_info(avg_size,max_size,max_size_index,P);
		int node=find_max_degree(max_size_index,P);
		P.remove_vertex(node,adjlist,row_length);
	}
	P.connectivity = get_connectivity(P.cur_solution->vlist,K);
}

void solve_cnp() {
	start_time = clock();
	initialization();
	int storm_time = 0;
	while (!timeout()) {
		/*-------------------------Particle swarm optimization-------------------------*/
		int stuck_time = 0;
		while(stuck_time!=STUCK_LIMIT&&!timeout()){
			if(local_search()){
				stuck_time = 0;
				if(min_connectivity<=f_best) {
					//_sleep(1000);
					return;
				}
			}else{
				stuck_time++;
				/*-----------------------------Storming phase-----------------------------*/
				storm_time++;
				for(int i=0;i<Par_Num;i++){
					//clear_weight(Particle[i]);
					shock(Particle[i],stuck_time);
				}
				// ral_clear(gBest);
				// gBest_connectivity = max_connectivity;
			}
		}
		// storm_time++;
		for(int i=0;i<Par_Num;i++){
			//shock(Particle[i],stuck_time);
			clear_weight(Particle[i]);
		}
	}
	//cout<<"storm time = "<<storm_time<<endl;
}

bool check(){
	int a = get_connectivity(best_solution,K);
	if(min_connectivity == a) return true;
	return false;
}

int main(int argc, char **argv) {
	char *File_name = new char[100];
	
	char s1[] ="./../benchmarks/cnd/";
	char s2[] ="./../benchmarks/RealInstances/";

	if(strcmp(argv[1],"M")==0){
		strcpy(File_name,s1);
	}
	else if(strcmp(argv[1],"R")==0){
		strcpy(File_name,s2);
	}
	strcat(File_name,argv[2]);
	//cout<<File_name<<endl;
	read_adjlist(File_name);  // argv[1]: input graph file
	if (!argv[3]) {
		printf("Please input a K\n");
		return 0;
	}
	K = atoi(argv[3]);  // argv[2]: K
	if (!argv[4]) {  // argv[3]: time limit
		printf("Defalut time_out = 3600\n");
		time_out = 3600;
	} else {
		time_out = atof(argv[4]);
	}
	int run_time = 30;
	if(!argv[5]){
		printf("Defalut repeat times = 30\n");
		run_time = 30;
	}else{
		run_time = atoi(argv[5]);
	}
	//int best_conncectivity = 0;
	if(argv[6]){
		f_best = atoi(argv[6]);
	}else{
		f_best = 0;
	}
	int global_best_conncectivity = MAXV*(MAXV-1);
	float sum_conncectivity=0;
	double count_best_times = 0;
	double sum_T = 0;
	cout<<"instance : "<<argv[2]<<endl;
	cout<<"|V| = "<<vertex_num<<" ; |E| = "<<edge_num<<" ;"<<endl;
	cout<<"Parameters: K = "<<K<<endl;
	cout<<"Limit time = "<<time_out<<endl;
	cout<<"Repeats = "<<run_time<<endl;
	cout<<"finish loading!"<<endl;
	int *g_best_solution = new int[K+2];
	int g_best_solution_con = vertex_num*(vertex_num-1)/2;

	for(int i=0;i<run_time;i++){
		local_search_time = 0;
		find_best_time = 0;
		find_best_step = 0;
		steps = 0;
		solve_cnp();
		sum_conncectivity+=min_connectivity;
		//if(min_connectivity<global_best_conncectivity) global_best_conncectivity = min_connectivity;
		if(global_best_conncectivity == min_connectivity){
			count_best_times++;
			time_avg +=find_best_time;
		}
		else if(min_connectivity<global_best_conncectivity){
			count_best_times = 1;
			time_avg =find_best_time;
			global_best_conncectivity = min_connectivity;
		}
		sum_T+=find_best_time;
		cout <<argv[2]<<" finish "<<i+1<<"-th trial"<<"; best_time  = "<<setprecision(5)<<find_best_time <<" ; best_obj_value =" << min_connectivity << ";" << endl;
		//cout <<argv[1]<<" : Reboot time: "<<i+1<<" ;find best time  = "<<setprecision(5)<<find_best_time <<" ;find best steps  = "<<find_best_step<<" ; best conncectivity =" << min_connectivity << ";" << endl;
		//if(!check()) cout<<"wrong!!!!!!!!!!!!!!"<<endl;
		delete [] Particle;
		if(min_connectivity<g_best_solution_con){
			g_best_solution_con = min_connectivity;
			for(int i=0;i<K;i++) g_best_solution[i]=best_solution[i];
		}
	}
	//cout<<"step avg = "<<sum_step/30<<endl;
	cout<<endl<<"------------------------------finish!------------------------------"<<endl;
	cout<<"instance : "<<argv[2]<<" ; Run Times : "<<run_time<<endl;
	//cout<<"BKV : "<<f_best<<endl;
	cout<<"found best objective : " <<global_best_conncectivity<<endl;
	cout<<"average value of the objective value : "<<fixed<<setprecision(4)<<sum_conncectivity/(double)run_time<<endl;
	cout<<"average time to find the objective value : "<<setprecision(5)<<sum_T/(double)run_time<<" s;"<<endl;
	cout<<"number of successful trials to find the best objective value = "<<setprecision(0)<<count_best_times<<endl;
	cout<<"average time to find the best objective value : "<<setprecision(5)<<time_avg/count_best_times<<endl;
	cout<<"-------------------------------------------------------------------"<<endl;
	cout<<"best solution :"<<endl;
	print_array(K,g_best_solution);
	delete[] best_solution;
	return 0;
}
