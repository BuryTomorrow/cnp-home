How to run the programs

MACNP:
./${ExecuteFile}" ${InstanceFile} ${Dataset} ${K} ${RunTime} ${NumberRepeats}

MACCCNP:
./${ExecuteFile}" ${InstanceFile} ${Dataset} ${L} ${RunTime} ${NumberRepeats}


// For example to run MACNP
// Given an instance, e.g., ErdosRenyi_n235.txt
DataSet = "model"
InstanceFile = "ErdosRenyi_n235.txt"
K=50
RunTime=3600
NumberRepeats=30